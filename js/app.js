"use strict";

var App = angular.module("todoList", ["ui.sortable", "LocalStorageModule"]);

App.controller("TodoCtrl", function ($scope, localStorageService) {

	$scope.init = function () {

		if (!localStorageService.get("todoList")) {
			$scope.model = [
				{
					name: "todo", list: []
				}
			];
		} else {
			$scope.model = localStorageService.get("todoList");
		}
		$scope.show = "All";
		$scope.currentShow = 0;
	};

	$scope.addTodo = function () {
		$scope.model[$scope.currentShow].list.splice(0, 0, {title: $scope.newTitle, description: $scope.newDescription, isDone: false });
		$scope.newTitle = "";
		$scope.newDescription = "";
	};

	$scope.deleteTodo = function (item) {
		var index = $scope.model[$scope.currentShow].list.indexOf(item);
		$scope.model[$scope.currentShow].list.splice(index, 1);
	};

	$scope.todoSortable = {
		containment: "parent",
		cursor: "move",
		tolerance: "pointer"
	};

	$scope.changeTodo = function (i) {
		$scope.currentShow = i;
	};

	$scope.sort = function (todo) {
		if ($scope.show === "All") {
			return true;
		}else if(todo.isDone && $scope.show === "Checked"){
			return true;
		}else if(!todo.isDone && $scope.show === "Unchecked"){
			return true;
		}else{
			return false;
		}
	};

	$scope.$watch("model",function (newVal,oldVal) {
		if (newVal !== null && angular.isDefined(newVal) && newVal!==oldVal) {
			localStorageService.add("todoList",angular.toJson(newVal));
		}
	},true);

});